class Node:
    def __init__(self, key = None, value = None):
        self.key = key
        self.value = value
        self.left = None
        self.right = None

class BinarySearchTree:
    def __init__(self):
        self.root = None

    #calculate size of BST
    def size(self):
        if self.root is None:
            return 0
        else:
            return self._size(self.root)
    
    def _size(self, current_node):
        leftSize, rightSize = 0, 0
        if current_node.left != None:
            leftSize = self._size(current_node.left)
        
        if current_node.right != None:
            rightSize = self._size(current_node.right)

        return (1 + leftSize + rightSize)

    #add key to the BST
    def add(self, key, value):
        if self.root is None:
            self.root = Node(key,value) 
        else:
            self._add(key,value, self.root)

    def _add(self, key,value, current_node):
        if key < current_node.key:
            if current_node.left is None:
                current_node.left = Node(key,value)
            else:
                self._add(key,value, current_node.left)
        elif key > current_node.key:
            if current_node.right is None:
                current_node.right = Node(key,value)
            else:
                self._add(key,value, current_node.right)
        else:
            print("The value inserted (",key,") is a duplicate")

    #search the BST for given key
    def search(self, key, return_instance = False):
        if self.root:
            is_found = self._search(key, self.root,return_instance)
            if(return_instance):
                return is_found
            if is_found:
                return is_found
            return False
        else:
            return None

    def _search(self, key, current_node,return_instance = False):
        if key > current_node.key and current_node.right:
            return self._search(key, current_node.right,return_instance)
        elif key < current_node.key and current_node.left:
            return self._search(key, current_node.left,return_instance)
        if key  == current_node.key:
            if(return_instance):
                return current_node
            return current_node.value

    #find the smallest node
    def smallest(self):
        if self.root:
            return self._smallest(self.root)        

    def _smallest(self, current_node):
        if current_node.left:
            return self._smallest(current_node.left)
        elif current_node.left == None:
            return(current_node.key,current_node.value)

    #print the largest node:
    def largest(self):
        if self.root:
            return self._largest(self.root)

    def _largest(self, current_node):
        if current_node.right:
            return self._largest(current_node.right)
        else:
            return(current_node.key,current_node.value)

    #remove given node
    def remove(self, key):
        target = self.search(key,True)
        current_node = target.right
        replacement = 0
        rparent = target
        while(True):
            if current_node.left != None:
                rparent =current_node
                curretn_node = current_node.left
                
            elif current_node.left == None:
                replacement = current_node
                break
        target.key = replacement.key
        target.value = replacement.value
        if(rparent.key == target.key):
            rparent.right = None
        else:
            rparent.left = None
        
    #print the nodes in inorder traversal
    def inorder_walk(self):
        if self.root:
            return self._inorder_walk(self.root)

    def _inorder_walk(self, current_node):
        if current_node:            
            l = []
            if(current_node.left):
                l=l+ self._inorder_walk(current_node.left)
            
            l=l+ [current_node.key]
            
            if(current_node.right):
                l=l+ self._inorder_walk(current_node.right)
            return l

    #print the nodes in preorder traversal
    def preorder_walk(self):
        if self.root:
            return self._preorder_walk(self.root)

    def _preorder_walk(self, current_node):
        if current_node:
            l = []
            l = l+[current_node.key]

            if(current_node.left):
                l=l+self._preorder_walk(current_node.left)

            if(current_node.right):
                l=l+self._preorder_walk(current_node.right)
            return l

    #print the nodes in postorder traversal
    def postorder_walk(self):
        if self.root:
            return self._postorder_walk(self.root)

    def _postorder_walk(self, current_node):
        if current_node:
            l = []

            if(current_node.left):
                l = l+self._postorder_walk(current_node.left)

            if(current_node.right):
                l= l+self._postorder_walk(current_node.right)

            l = l+[current_node.key]
            return l